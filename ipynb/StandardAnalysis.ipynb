{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ASL Analysis (pCASL with Multiple Delays)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the process for the analysis of multiple post-labelling delay measurements as done by Tom Okell on data acquired at Oxford. It uses the FSL tool for ASL analysis: [BASIL](http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/BASIL), in particular the driver script `oxford_asl`.\n",
    "\n",
    "This is a multiple T<sub>I</sub> (ie post-labelling delay) experiment with each one acquired separately so we shall have several distinct pCASL series, one for each delay. There will also be an M<sub>0</sub> acquisition with both the pCASL labelling and the background suppression turned off (the T<sub>R</sub> is also longer). We can acquire the M<sub>0</sub> images with 'prescan normalize' and 'keep original images' options selected so as to obtain an additional reference image free of coil non-uniformities.\n",
    "\n",
    "The series for the different delay times are stacked together into a 4D (3D+T) volume using `fslmerge` to get a single file nifti dataset.\n",
    "\n",
    "We have modified the default `oxford_asl` analysis as follows:\n",
    "- Use `mcflirt` to do motion correction of the asl series.\n",
    "- Run `BET` explicity on the MPRAGE and on the M<sub>0</sub>\n",
    "- Use `flirt` to a generate transformation matrices between the `BET`ed MPRAGE and M<sub>0</sub> and generate a csf mask from this\n",
    "- Run `oxford_asl` with an explicit expected bolus arrival time of 1.3 or 1.4 s rather than the old default of 0.7 which was really for pulsed ASL\n",
    "- Used spatial regularisation in `oxford_asl` as suggested by Tom to reduce vascular contamination\n",
    "\n",
    "We've also changed the acquisition protocol slightly by increasing the labelling duration from 1400 ms to 1800 ms to match the white paper value but as this value is determined from the scans it doesn't affect the analysis here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import os\n",
    "# Uncomment one of the following lines to explicitly force fsl5 or fsl6 rather than using the default\n",
    "#os.environ['PATH'] = ':'.join(['/usr/local/fsl5/bin', os.environ['PATH']])\n",
    "#os.environ['PATH'] = ':'.join(['/usr/local/fsl6/bin', os.environ['PATH']])\n",
    "\n",
    "# Patient details file yaml/json\n",
    "import yaml\n",
    "from os.path import join\n",
    "\n",
    "# Plotting etc\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "plt.style.use('bmh')\n",
    "\n",
    "import numpy as np\n",
    "\n",
    "# Image read/write\n",
    "import nibabel as nib\n",
    "\n",
    "import pandas as pd\n",
    "\n",
    "from IPython.display import HTML\n",
    "\n",
    "# Helper functions for pcasl\n",
    "import pcasl\n",
    "\n",
    "# Record software versions used\n",
    "# Uncomment the following line to install the required extension if need be\n",
    "#%install_ext https://raw.githubusercontent.com/rasbt/python_reference/master/ipython_magic/watermark.py\n",
    "print()\n",
    "%load_ext watermark\n",
    "%watermark\n",
    "print()\n",
    "!printf 'Using oxford_asl '; oxford_asl --version\n",
    "!printf 'Using asl_calib '; asl_calib --version"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Definitions of the Series to be Analysed\n",
    "\n",
    "The details of the scans on the [CRIC DICOM server](http://canopus.cric.bris.ac.uk/dcm4chee-web/) are specified in a [yaml](https://en.wikipedia.org/wiki/YAML) text file in the same directory as this notebook. The series numbers corresponding to the various scans are read from this file so the same notebook can be used for the analysis of different studies without modification. \n",
    "\n",
    "This is a multiple $T_I$ experiment so we shall have more than one pCASL series. There will also be an $M_0$ acquisition with the pCASL labelling and background suppression turned off (the $T_R$ is also longer). This is acquired with prescan normalise enabled but with the option to keep the original images. The original images will be used as the $M_0$ and the corrected images used to apply a non-uniformity correction in the analysis without having to acquire a separate body coil series for this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "#\n",
    "# Get the details of the Patient/Study/Series we are interested in from external file.\n",
    "#\n",
    "study_info_file = 'study_info.yaml'\n",
    "\n",
    "with open(study_info_file) as fp:\n",
    "    study_info = yaml.safe_load(fp)\n",
    "\n",
    "study_title = study_info['Title']\n",
    "\n",
    "casl_patid       = study_info['CASLPatientID']\n",
    "casl_studyid     = study_info['CASLStudyID']\n",
    "casl_studydate   = study_info['CASLDate']\n",
    "casl_series      = study_info['CASLSeriesNumbers']\n",
    "mprage_patid     = study_info['AnatPatientID']\n",
    "mprage_studyid   = study_info['AnatStudyID']\n",
    "mprage_studydate = study_info['AnatDate']\n",
    "mprage_series    = study_info['AnatSeriesNumber']\n",
    "\n",
    "m0_series, m0norm_series = study_info['M0SeriesNumber'], study_info['M0NORMSeriesNumber']\n",
    "\n",
    "display(HTML('<H2><center>%s</center></H2>' % study_title))\n",
    "pd.DataFrame([study_info]).T.rename(columns={0:'Study details'})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll fetch the relevant series from the DICOM server and convert them to [nifti](http://nifti.nimh.nih.gov/) format for the FSL analysis tools. We'll also extract some relevant acquisition parameters while we have the dicom objects so we can provide these to the FSL analysis script. The images may be in mosaic form (newer version of the pcasl sequence), but nifti conversion program [dcm2niix](https://github.com/neurolabusc/dcm2niix) used by the fetch routines should handle this OK."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "#\n",
    "# Fetch scans from DICOM server\n",
    "#\n",
    "server = 'Dcm4Chee'                                                        \n",
    "series_dict = {}\n",
    "\n",
    "workingdir = casl_patid.replace(' ', '')  + '-%s' % casl_studyid + '-%d' % casl_series[0]\n",
    "assert workingdir\n",
    "!rm -rf ./{workingdir}\n",
    "!mkdir ./{workingdir}\n",
    "\n",
    "print('Fetching', '..', end=' ')\n",
    "series_dict['Anatomy'] = pcasl.fetch_structural(\n",
    "    server=server, patid=mprage_patid, studyid=mprage_studyid, seriesno=mprage_series,\n",
    "    filename='MPRAGE', workingdir=workingdir, isodate=mprage_studydate\n",
    ")\n",
    "print('MPRAGE', '..', end=' ')\n",
    "\n",
    "series_dict['CASL'] = pcasl.fetch_casl(\n",
    "    server=server, patid=casl_patid, studyid=casl_studyid, seriesnos=casl_series,\n",
    "    filenames=['CASL%02d' % i for i in range(len(casl_series))], workingdir=workingdir, isodate=casl_studydate\n",
    ")\n",
    "print('CASL', '..', end=' ')\n",
    "\n",
    "series_dict['M0'] = pcasl.fetch_m0(\n",
    "    server=server, patid=casl_patid, studyid=casl_studyid, seriesno=m0_series,\n",
    "    filename='M0', workingdir=workingdir, isodate=casl_studydate\n",
    ")\n",
    "print('M0', '..', end=' ')\n",
    "\n",
    "series_dict['M0NORM'] = pcasl.fetch_m0norm(\n",
    "    server=server, patid=casl_patid, studyid=casl_studyid, seriesno=m0norm_series,\n",
    "    filename='M0NORM', workingdir=workingdir, isodate=casl_studydate\n",
    ")\n",
    "print('M0NORM', '..', end=' ')\n",
    "\n",
    "print('Done.')\n",
    "\n",
    "print('Using Anatomical Patient ID %s, Study ID %s and  Series %s' %\n",
    "      (mprage_patid, mprage_studyid, series_dict['Anatomy']['SeriesList'][0])\n",
    ")\n",
    "print('Using CASL Patient ID %s, Study ID %s and pCASLs starting from series %s' %\n",
    "      (casl_patid, casl_studyid, series_dict['CASL']['SeriesList'][0])\n",
    ")\n",
    "print('Analysis Working Directory is', workingdir)\n",
    "\n",
    "# Save scan parameter information in yaml file for use in further analyses\n",
    "with open(join(workingdir, 'series_dict.yaml'), 'w') as f:\n",
    "    yaml.dump(series_dict, f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Oxford_asl was modified in `FSL 5.0.6` to make it more consistent with the ASL consensus paper. Thus T<sub>1</sub> values differ from the earlier releases and the inversion efficiency is now included by default (this can be 'turned off' by setting --alpha=1). It now also expects all structural images to have already been brain extracted (in the past `bet` was run internally). This now leaves it up to the user to choose their preferred `bet` options and get the brain extraction they wish without any further modification by `oxford_asl`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preparing the Images and Checking the Acquisition Parameters\n",
    "Each pCASL series is acquired with just two phases, that is it is a conventional `tag` vs `control` experiment. The measurements in the time series alternate between the two conditions so in order to get the raw ASL images we subtract adjacent pairs of time points and then average up all these differences into a single (spatial) series.\n",
    "\n",
    "First though, we'll perform a `mcflirt` intra-series motion correction to each pCASL series. If we merge the series first then we can perform the registration across all the different $T_I$s as well."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Check we have the right file names and delay times"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "casl_files   = series_dict['CASL']['FileNames']\n",
    "ncasl_files  = len(casl_files)\n",
    "casl_paths   = ' '.join([os.path.join(workingdir, filename) for filename in casl_files])\n",
    "m0_file      = series_dict['M0']['FileNames'][0]\n",
    "m0_norm_file = series_dict['M0NORM']['FileNames'][0]\n",
    "mprage_file  = series_dict['Anatomy']['FileNames'][0]\n",
    "\n",
    "print('Post-labelling delays:')\n",
    "print([(f, sp['PLD']) for f, sp in zip(casl_files, series_dict['CASL']['ScanParameters'])])\n",
    "print('M0 files:', m0_file, m0_norm_file)\n",
    "print('Anatomy file:', mprage_file)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Merge the pCASL series for different delay times into a single 4D series."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then need to construct a pseudo 4D data set by stacking up the different pCASL flow images along the existing time axis in order of their $T_I$ values. The length of the new series in time wil be the sum of the number of time points at each $T_I$. We assume that the alphabetical ordering above of the filenames of the different series corresponds to increasing post-labelling delay time as we have arranged for that to be true in the cells above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "!fslmerge -t $workingdir/ep2d_casl_merged $casl_paths\n",
    "print('Done. Output is in %s' % os.path.join(workingdir, 'ep2d_casl_merged'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Apply the motion correction across all the series."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we perform a `mcflirt` intra-series motion correction. Using a value of 4 for the `stages` parameter (rather than the default 3) implies a final sinc interpolation - I'm not clear if this is of any benefit. The `-plots` flag produces a `.par` output file with the motion parameters and we can plot that here to check there is no large movements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# Mcflirt registration\n",
    "!mcflirt -in $workingdir/ep2d_casl_merged.nii.gz -stages 4 -out $workingdir/ep2d_casl_merged_MC -plots\n",
    "\n",
    "# Use pandas i/o and Dataframe plotting\n",
    "df = pd.read_csv(os.path.join(workingdir, 'ep2d_casl_merged_MC.par'),\n",
    "            delim_whitespace=True, header=None, names=('rx', 'ry', 'rz', 'dx', 'dy', 'dz'))\n",
    "\n",
    "fig, axs = plt.subplots(nrows=2, ncols=1, figsize=(15, 5))\n",
    "\n",
    "np.degrees(df[['rx', 'ry', 'rz']]).plot(ax=axs[0], title='Rotations (degrees)')\n",
    "axs[0].grid(True)\n",
    "\n",
    "df[['dx', 'dy', 'dz']].plot(ax=axs[1], title='Shifts (mm)');\n",
    "axs[1].grid(True)\n",
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The movements here should be relatively small and the dominant components should be slow drifts rather than abrupt changes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Tag - Control Subtraction and Averaging"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we'll go on and do the paired subtraction and averaging in each of the series we've just motion corrected. For this we'll use the FSL tool `asl_file`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# Mean of paired differences for 2 point tag/control experiment\n",
    "!asl_file --data=$workingdir/ep2d_casl_merged_MC --ntis=$ncasl_files --ibf=tis --iaf=tc --diff --mean=$workingdir/ep2d_casl_merged_MC_diff_mean\n",
    "print('Output is in %s' % os.path.join(workingdir, 'ep2d_casl_merged_MC_diff_mean'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Check Sequence Parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to perform the standard Oxford ASL analysis using the `oxford_asl` script we really need some of the acquisition parameters from the pCASL and $M_0$ calibration series. As we extracted the values when we downloaded the DICOM files we can get these directly. If we didn't have the DICOM files then most of this would have to be set manually as it is not present in the NIFTI headers.\n",
    "\n",
    "Note that the $T_I$s, $T_1$s, $T_R$s and slice delays are all expected in seconds but the $T_E$ (and the $T_2^*$ values of tissues if specified) are in milliseconds (I verified this by inspecting FSL source code). We'll need to take this into account when calling the fsl routines.\n",
    "\n",
    "First, let's check that the parameters are all consistent for the different $T_I$s:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "#\n",
    "# Check consistency of sequence parameters across multiple TIs\n",
    "#\n",
    "print('Sequence parameters across different delay times:')\n",
    "print('TEs     =', ['%0.1f' % sp['EchoTime']         for sp in series_dict['CASL']['ScanParameters']], 'ms')\n",
    "print('TRs     =', ['%0.1f' % sp['RepetitionTime']   for sp in series_dict['CASL']['ScanParameters']], 'ms')\n",
    "print('Delays  =', ['%0.3f' % sp['SliceDelay']       for sp in series_dict['CASL']['ScanParameters']], 'ms')\n",
    "print('T1Opts  =', ['%0.1f' % sp['T1Opt']            for sp in series_dict['CASL']['ScanParameters']], 'ms')\n",
    "print('PreSat  =', ['On' if sp['PreSat']  else 'Off' for sp in series_dict['CASL']['ScanParameters']])\n",
    "print('MppcASL =', ['On' if sp['PerformMppcASL'] else 'Off' for sp in series_dict['CASL']['ScanParameters']])\n",
    "print('DInv    =', ['On' if sp['DInv']    else 'Off' for sp in series_dict['CASL']['ScanParameters']])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we'll get the parameters that we are going to use in the analysis. Note that `oxford_asl` expects pulsed ASL $T_I$ values rather than our pCASL post labelling delays. We add in the labelling duration to the post-labelling delays to get the times from the *start* rather than the *end* of the labelling up to the centre of the acquisition and use this as the $T_I$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# Extract sequence parameters (attached to series_dict)\n",
    "delay_times = [sp['PLD'] for sp in series_dict['CASL']['ScanParameters']]\n",
    "tis    = [sp['TagDur'] + sp['PLD'] for sp in series_dict['CASL']['ScanParameters']]\n",
    "taglen = series_dict['CASL']['ScanParameters'][0]['TagDur']\n",
    "\n",
    "# Convert the times from millisecs to secs and format them for the oxford_asl command line\n",
    "tiss = ','.join(['%-.2f' % round(ti/1000.0, 3) for ti in tis])\n",
    "bolus = '%-.2f' % round(taglen/1000.0, 3)\n",
    "\n",
    "te = '%-.1f' % round(series_dict['M0']['ScanParameters'][0]['EchoTime'], 1) # ms\n",
    "tr = '%-.2f' % round(series_dict['M0']['ScanParameters'][0]['RepetitionTime'] / 1000.0, 4) # s\n",
    "dt = '%-.4f' % round(series_dict['M0']['ScanParameters'][0]['SliceDelay'] / 1000.0, 4) # s\n",
    "\n",
    "print('We are using the following pCASL parameters:')\n",
    "print('Inversion Times   (secs): ', tiss)\n",
    "print('Bolus Duration    (secs): ', bolus)\n",
    "print('Inter-Slice Time  (secs): ', dt)\n",
    "print('Echo Time    (millisecs): ', te)\n",
    "\n",
    "print('PreSat BG Suppresion    : ', 'On' if series_dict['CASL']['ScanParameters'][0]['PreSat'] else 'Off')\n",
    "print('pCASL Labelling         : ', 'On' if series_dict['CASL']['ScanParameters'][0]['PerformMppcASL'] else 'Off')\n",
    "print('Double Inversion BGS    : ', 'On' if series_dict['CASL']['ScanParameters'][0]['DInv'] else 'Off')\n",
    "print()\n",
    "print('In addition, we have for the M0 calibration:')\n",
    "print('Repetition Time   (secs): ', tr)\n",
    "print('PreSat BG Suppresion    : ', 'On' if series_dict['M0']['ScanParameters'][0]['PreSat'] else 'Off')\n",
    "print('pCASL Labelling         : ', 'On' if series_dict['M0']['ScanParameters'][0]['PerformMppcASL'] else 'Off')\n",
    "print('Double Inversion BGS    : ', 'On' if series_dict['M0']['ScanParameters'][0]['DInv'] else 'Off')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Single Delay Time Images"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll have a look at the raw subtraction data for the different delay times individually. Each image is the same slice location but at a different delay time. You can see what looks like the arterial component dominating in the early frames."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# Plot example images for different post-labelling delay times\n",
    "casl_image = nib.load(os.path.join(workingdir, 'ep2d_casl_merged_MC_diff_mean.nii.gz'))\n",
    "d = casl_image.get_fdata()\n",
    "nslices = d.shape[-2]\n",
    "ims = d[:, ::-1, nslices//2 + 4, :].T\n",
    "\n",
    "vmin, vmax = np.amin(ims), np.amax(ims)\n",
    "\n",
    "nslices = len(ims)\n",
    "ncols = 3\n",
    "nrows = int(np.ceil(nslices / float(ncols)))\n",
    "\n",
    "fig, axs = plt.subplots(nrows, ncols, figsize=(4*ncols + 1, 4*nrows))\n",
    "implots = []\n",
    "for j in range(nrows):\n",
    "    for i in range(ncols):\n",
    "        delay = i + j*ncols\n",
    "        try:\n",
    "            implots.append(axs[j][i].imshow(ims[delay], cmap='viridis', vmin=0, vmax=vmax/2))\n",
    "            axs[j][i].set_title('%d ms' % delay_times[delay])\n",
    "        except IndexError:\n",
    "            implots.append(axs[j][i].imshow(np.zeros(ims[0].shape), cmap='viridis', vmin=0, vmax=vmax/2))\n",
    "        axs[j][i].axis('image')\n",
    "        axs[j][i].axis('off')  \n",
    "fig.subplots_adjust(right=0.8)\n",
    "cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])\n",
    "fig.colorbar(implots[0], cax=cbar_ax)\n",
    "fig.suptitle(\"Raw ASL Maps for Different pCASL Delay Times\", fontsize=24);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The tag gets quite faint by the longest delay. There's probably no need to go to any longer delay times."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Running the Oxford ASL Analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Initially we ran the main Oxford analysis script `oxford_asl` directly at this point. The bash script is a wrapper for several FSL programs including the primary ASL fitting program `BASIL` and the calibration program `asl_cal`. These perform the fitting of a Bayesian model to the ASL data and use the $M_0$ images to calibrate the results. It is intended to yield a data set in absolute units of flow volume per mass of tissue (ml/100g/min) .\n",
    "\n",
    "However, in Tom's analysis the calibration and the generation of the mask required to define calibration reference tissue (csf) are both performed separately before using `oxford_asl`. This involves running `BET` on the anatomical images first so as to get a better mask as, previously, `oxford_asl` was generating a very poor csf mask. We'll do that first. In the current version of `oxford_asl` this is what is expected. For the structural image Tom uses `-B` for bias field correction and a lower than default threshold (0.3 vs 0.5). For the $M_0$ he uses the defaults. He then uses `flirt` to register the structural and the $M_0$, getting asl to structural and structural to asl transformation matrices. This is exactly as in the bespoke script `to_ox_asl`. Note that `BET` takes quite a long time to run here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Generation of a Mask for the Reference Tissue"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Average each of the $M_0$ series"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "!fslmaths $workingdir/$m0_file      -Tmean $workingdir/M0_head\n",
    "!fslmaths $workingdir/$m0_norm_file -Tmean $workingdir/M0_norm\n",
    "print('Done')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Derive transforms between the $M_0$ and structural spaces"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Brain extract the M0 and the structural scans.\n",
    "# BET parameters: -B: bias field and neck clean up -f: fractional intensity threshold -g: vert grad in frac intens thresh.\n",
    "print('BET', '..', end=' ')\n",
    "!bet $workingdir/$mprage_file $workingdir/MPRAGEBET -B -f 0.3 -g 0\n",
    "\n",
    "# We'll use the normalised m0 as this should give better brain extraction and registration\n",
    "!bet $workingdir/M0_norm $workingdir/M0_norm_BET\n",
    "\n",
    "print('Flirt', '..', end=' ')\n",
    "# Rigid registration of structural and M0. Get transformation matrices both ways.\n",
    "!flirt -in $workingdir/M0_norm_BET \\\n",
    "       -ref $workingdir/MPRAGEBET \\\n",
    "       -omat $workingdir/asl2struct.mat \\\n",
    "       -out $workingdir/CalibHead_bet_StructSpace_not_used \\\n",
    "       -dof 6 \\\n",
    "       -searchrx -30 30 \\\n",
    "       -searchry -30 30 \\\n",
    "       -searchrz -30 30\n",
    "\n",
    "# This is all we are really interested in - the transformation matrix\n",
    "!convert_xfm -omat $workingdir/struct2asl.mat -inverse $workingdir/asl2struct.mat\n",
    "\n",
    "print('Done')\n",
    "\n",
    "# Check the rotations are reasonable\n",
    "def euler_angles(M):\n",
    "    cy = np.hypot(M[0, 0], M[1, 0])\n",
    "    if cy > 4 * np.finfo(float).eps:\n",
    "        return np.arctan2( M[2, 1],  M[2, 2]), np.arctan2(-M[2, 0], cy), np.arctan2(M[1, 0],  M[0, 0])\n",
    "    else:\n",
    "        return np.arctan2(-M[1, 2],  M[1, 1]), np.arctan2(-M[2, 0], cy), 0.0\n",
    "    \n",
    "tform = os.path.join(workingdir, 'asl2struct.mat')\n",
    "angles = tuple(np.degrees(euler_angles(np.loadtxt(tform))))\n",
    "HTML('Rotation Angles: [&alpha;=%0.1f&deg;, &beta; = %0.1f&deg;, &gamma; = %0.1f&deg;]' % angles)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Calibration to Reference Tissue"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll also do the calibration phase here separately using `asl_calib` directly rather than in `oxford_asl`. We can do it first and pass the calibrated dataset to `oxford_asl`. This may no longer be required in the current version of `oxford_asl` but we'll keep it here for now.\n",
    "\n",
    "The calibration requires a reference tissue - in this case specified as *csf*. We assume $T_1$ and $T_2$ values for the *csf* of 4.3 s and 400 ms respectively. Using a uniform image (the head coil with prescan-normalisation) as a reference we can also output a coil sensitivity map to be used later. We'll use the prescan-normalised head coil image as this can be obtained togther with the $M_0$ image without a separate scan.\n",
    "\n",
    "Note there is no partial volume correction, though that *is* an option to Tom's script `to_ox_asl`.\n",
    "\n",
    "The options to `asl_calib` are taken from `to_ox_asl`.\n",
    "\n",
    "The exact command line used in Tom's script was:\n",
    "```\n",
    "asl_calib -c $CALIBH -s $STRUCTBET -t $ASL2STRUCT --mode longtr --tissref csf \\\n",
    "    --te 0.014 -o $OUTDIR --t1r 4.3 --t2r 0.750 --t2b 0.050 --tr $CALIBTR --cgain 1 \\\n",
    "    --cref $CALIBB --osen $OUTDIR/sens \n",
    "```\n",
    "\n",
    "where `$CALIBB` is the un-`BET`ed body coil image.\n",
    "\n",
    "Note that, as in `to_ox_asl`, we are using the `BET`ed version of the MPRAGE dataset but the non-`BET`ed $M_0$ images."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# Assumed values\n",
    "t1_csf = 4.3 # seconds\n",
    "t2_csf = 400 # ms\n",
    "t2_blood = 50 # (or t2star in this case) ms\n",
    "\n",
    "# NB T1/TR values are in seconds and T2/TE values in milliseconds \n",
    "asl_calib = f\"asl_calib -c {workingdir}/M0_head \\\n",
    "           -s {workingdir}/MPRAGEBET \\\n",
    "           -t {workingdir}/asl2struct.mat \\\n",
    "           --mode longtr \\\n",
    "           --tissref csf \\\n",
    "           --te {te} \\\n",
    "           -o {workingdir} \\\n",
    "           --t1r {t1_csf} --t2r {t2_csf} --t2b {t2_blood} \\\n",
    "           --tr {tr} \\\n",
    "           --cgain 1 \\\n",
    "           --cref {workingdir}/M0_norm \\\n",
    "           --osen {workingdir}/sensitivity_map\"\n",
    "\n",
    "print(\"Running the script 'asl_calib' as follows:-\")\n",
    "!echo $asl_calib\n",
    "     \n",
    "print(\"This may take some time ...\")\n",
    "!$asl_calib\n",
    "\n",
    "m0 = float(np.loadtxt(os.path.join(workingdir, 'M0.txt')))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Take a look at the resulting csf mask overlaid on the normalized $M_0$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "pcasl.show_mosaic(\n",
    "    os.path.join(workingdir, 'M0_norm.nii.gz'),\n",
    "    os.path.join(workingdir, 'refmask.nii.gz'), mask=True,\n",
    "    title='CSF Reference Mask'\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The mask may seem quite small but it should lie within the ventricles."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Calibrate ASL subtraction data\n",
    "Now, we'll manually apply the calibration results to the ASL subtraction data, correcting along the way for $M_0$, the labelling efficiency &alpha; and the coil sensitivity variations. The final result should be in the appropriate physiological units of ml/100g/min. Newer versions of `oxford_asl` uses 85% for the labelling efficiency so we'll stick with that here. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "alpha = 0.85 # as in oxford_asl 1.41\n",
    "print('Calibrating assuming labelling efficiency alpha = %d%%' % (alpha*100))\n",
    "!fslmaths $workingdir/ep2d_casl_merged_MC_diff_mean.nii.gz \\\n",
    "          -div $workingdir/sensitivity_map \\\n",
    "          -mas $workingdir/sensitivity_map \\\n",
    "          -div $m0 -mul 60 -mul 100 -div $alpha \\\n",
    "          $workingdir/perf_calib "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Brain Mask"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll also pre-calculate a mask image based on our skull stripped images."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# Calculate a whole brain mask from the structural image mapped into asl space\n",
    "!fslmaths $workingdir/perf_calib -Tmean $workingdir/perf_calib_mean\n",
    "!flirt -in $workingdir/MPRAGEBET -ref $workingdir/perf_calib_mean -out $workingdir/mask -init $workingdir/struct2asl.mat -applyxfm -paddingsize 0.0 -interp trilinear \n",
    "\n",
    "# and make it binary\n",
    "!fslmaths $workingdir/mask -bin $workingdir/mask\n",
    "print('Brain Mask %s' % os.path.join(workingdir, 'mask'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Run oxford_asl to do the analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we'll run the standard `oxford_asl` script but we'll input the already calibrated data and our own derived mask. Most of the other parameters have been extracted above from the DICOM images. An exception is the bolus arrival time. Tom's script `to_ox_asl` uses a prior for the bolus arrival time of 1.4 seconds. The problem we were having before with the arrival time was probably because the defaults in oxford_asl were too short so we've set it to 1.3 s here with the -bat option as this is the revised default in `oxford_asl` as of FSL 5.0.7. In addition we'll use regularisation with a spatial prior by specifying the `--spatial` flag. This is to try and reduce arterial contamination."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "bolus_arrival_time = 1.3 # secs as in oxford_asl 1.41\n",
    "\n",
    "oxford_asl = f\"oxford_asl \\\n",
    "      -i {workingdir}/perf_calib \\\n",
    "      --tis {tiss} \\\n",
    "      -o {workingdir}/perf_quant \\\n",
    "      --casl \\\n",
    "      --bolus {bolus} \\\n",
    "      --slicedt {dt} \\\n",
    "      --fixbolus \\\n",
    "      --bat {bolus_arrival_time} \\\n",
    "      --spatial \\\n",
    "      -m {workingdir}/mask\"\n",
    "\n",
    "print(\"Running the script 'oxford_asl' as follows:-\")\n",
    "!echo $oxford_asl\n",
    "\n",
    "print()\n",
    "\n",
    "print(\"This may take some time ...\")\n",
    "!$oxford_asl"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Review of Results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The arrival times should show a grey/white difference."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "pcasl.show_mosaic(\n",
    "    os.path.join(workingdir, 'perf_quant', 'native_space', 'arrival.nii.gz'),\n",
    "    cmap='bone',\n",
    "    colourbar=True,\n",
    "    vmax=2.25,\n",
    "    title=\"Arrival Time\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The resulting calibrated perfusion image can be examined in detail using `fsleyes` but here we'll just use the python/matplotlib to display an overview."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "pcasl.show_mosaic(\n",
    "    os.path.join(workingdir, 'perf_quant', 'native_space', 'perfusion.nii.gz'),\n",
    "    cmap='viridis',\n",
    "    colourbar=True,\n",
    "    vmax=150,\n",
    "    title=\"Calibrated Perfusion Maps from Multiple Delay Time pCASL\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Discussion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These shouldn't look too lumpy - the spatial regularisation helps with this. The maximum values should be compared to the physiologically plausible values of 80-100 ml/100g/min."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# Uncomment to view in external fsleyes viewer\n",
    "#!fsleyes {workingdir}/perf_quant/native_space/perfusion.nii.gz"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
