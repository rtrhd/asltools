# Asltools #

This is some initial work on the analysis of ASL data at CRICBristol using the FSL toolchain. There are example [Jupyter](http://jupyter.org/) notebooks of the analysis workflow for the pCASL data acquired using the CRIC port of Tom Okell's Oxford pCASL sequence.

## Requirements ##
 - A scientific python distribution (python >=3.5) including Jupyter Notebook server (best is [Anaconda](https://www.anaconda.com/downloads))
 - The [pydicom](http://www.pydicom.org/) package (pip or conda-forge)
 - The [nibabel](http://nipy.org/nibabel/) package (pip or conda-forge)
 - The [dcmfetch](https://bitbucket.org/rtrhd/dcmfetch) package (pip installable fron bitbucket repo)
 - The [dcmextras](https://bitbucket.org/rtrhd/dcmextras) package (pip installable fron bitbucket repo)
 - Oxford [fsl](http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/) (installed via neurodebian or directly)
 - [dcm2niix](https://github.com/neurolabusc/dcm2niix) (installed via neurodebian or directly)

DICOM access to a server holding the MR images is needed. The notebooks are written to run against the DICOM server on `canopus` at CRIC which is accessible via the UoB Junos VPN, but any DICOM server such as [Orthanc](https://www.orthanc-server.com/) would be suitable.

Most of this is just the Jupyter notebooks but it relies on some utility routines provided in the pcasl module.

This is a placeholder for any further work on pCASL analysis pipelines at CRIC.

R. Hartley-Davies, June 2018


