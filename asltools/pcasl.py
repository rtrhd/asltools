#
# Support routines for pcasl analysis. (2nd attempt RHD 01/12/2015)
#
from __future__ import print_function, division

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from glob import glob
from os.path import join
from shutil import rmtree, copyfile, move
from tempfile import mkdtemp
from subprocess import check_call
from operator import attrgetter

try:
    from pydicom import dcmread
except ImportError:
    from dicom import read_file as dcmread

from dcmextras.siemenscsa import csa, phoenix
from dcmfetch.queryinterface import QueryInterface
import nibabel as nib


def _convert_to_nifti(tempdir, program='dcm2niix', options=('-z', 'y', '-f', '%t_%p_%s')):
    '''Convert dicom to nifti using external program.

     Format options is specified so that filenames begin with acquisition time field
     so they will subsequently sort in time order.
    '''
    check_call([program] + list(options) + [tempdir])


def _merge_to_timeseries(fromdir, todir, filename, pattern="*.nii.gz", program='fslmerge', options=("-t",)):
    '''Assemble multiple nifti image series into 4D nifti time series.

    Assumes filenames sort in time order - fslmerge blindly assembles the time series
    according to the order in which the files are specified on the command line.
    '''
    files_to_merge = sorted(glob(join(fromdir, pattern)))
    check_call(
        [program] +
        list(options) +
        [join(todir, filename)] +
        files_to_merge
    )


# TODO: Note this is extremely sensitive to the version of the pcasl sequence
# so we should try and make this check the version if we can find a way to
# pass this through from the sequence somehow - maybe via a wip text field.
def _pcasl_wipparam(dobj, tagname):
    '''pCASL WIP parameter from (a protocol in) a dicom object.

    Parameters
    ----------
    dobj :
        dicom object.
    tagname : name of tag as defined here

    '''

    tags = {
        'PerformMppcASL': ('sWipMemBlock.alFree[%d]' % 0,  bool),
        'NumPhases':      ('sWipMemBlock.alFree[%d]' % 1,  int),
        'RunSpoil':       ('sWipMemBlock.alFree[%d]' % 2,  bool),
        'RFFlip':         ('sWipMemBlock.alFree[%d]' % 4,  int),
        'RFDur':          ('sWipMemBlock.alFree[%d]' % 5,  int),
        'RFSep':          ('sWipMemBlock.alFree[%d]' % 6,  int),
        'MeanGrad':       ('sWipMemBlock.adFree[%d]' % 7,  float),
        'TagGrad':        ('sWipMemBlock.adFree[%d]' % 8,  float),
        'TagDur':         ('sWipMemBlock.adFree[%d]' % 9,  float),
        'PLD':            ('sWipMemBlock.adFree[%d]' % 10, float),
        'T1Opt':          ('sWipMemBlock.adFree[%d]' % 11, float),
        'PreSat':         ('sWipMemBlock.alFree[%d]' % 12, bool),
        'DInv':           ('sWipMemBlock.alFree[%d]' % 13, bool)
    }
    protocol = phoenix(dobj)
    # Bizarre: if a boolean is False it just disappears
    try:
        return tags[tagname][1](protocol[tags[tagname][0]])
    except KeyError as e:
        if tags[tagname][1] == bool:
            return False
        elif tags[tagname][1] == int:
            return 0
        else:
            raise e


def pcasl_params(dobjs):
    '''A dictionary of useful pCASL acquisition parameters from a list of dicom objects.

    Parameters
    ----------
    dobjs :
        a list of dicom objects.
    '''

    # Siemens Private Tags
    TimeAfterStart    = (0x0019, 0x1016)
    MosaicRefAcqTimes = (0x0019, 0x1029)
    BandwidthPerPixelPhaseEncode = (0x0019, 0x1028)

    assert all(dobj.Manufacturer.strip() == 'SIEMENS' for dobj in dobjs)

    # Sort by both acquisition number and slice position
    # (if these are mosaics then the slice positions will nominal and equal for all)
    series = sorted(dobjs, key=lambda d: (int(d.AcquisitionNumber), float(d.SliceLocation)))

    def _pfmap(siemenscode):
        # Partial fourier bit patterns from MR/public/Protbasic/Interfaces/KSpacedefines.h
        return {
            32: 0.0,   # "AUTO" so undefined from protocol
            16: 8.0/8, # OFF
             8: 7.0/8, # 7_8
             4: 6.0/8, # 6_8
             2: 5.0/8, # 5_8
             1: 4.0/8 # HALF
        }[siemenscode]

    # Take first object as representative
    img = series[0]  # still ok for mosaic
    params = {
        'PreSat':         _pcasl_wipparam(img, 'PreSat'),
        'PerformMppcASL': _pcasl_wipparam(img, 'PerformMppcASL'),
        'DInv':           _pcasl_wipparam(img, 'DInv'),
        'PLD':            _pcasl_wipparam(img, 'PLD'),     # millisecs
        'TagDur':         _pcasl_wipparam(img, 'TagDur'),  # millisecs
        'RepetitionTime': float(img.RepetitionTime),       # millisecs
        'EchoTime':       float(img.EchoTime),             # millisecs
        'T1Opt':          _pcasl_wipparam(img, 'T1Opt'),
        'InPlanePhaseEncodingDirection': str(img.InPlanePhaseEncodingDirection),  # ROW|COL
        'BandwidthPerPixelPhaseEncode': float(img[BandwidthPerPixelPhaseEncode].value),
        'PhaseEncodingDirectionPositive': bool(int(csa(img, 'Image')['PhaseEncodingDirectionPositive'])),
        'AcquisitionMatrix': list(map(int, img.AcquisitionMatrix)),
        'EchoTrainLength': int(csa(img, 'Series')['EchoTrainLength']),
        'PhasePartialFourier': _pfmap(phoenix(img)['sKSpace.ucPhasePartialFourier']),
        'AccelFactPE': int(phoenix(img)['sPat.lAccelFactPE'])
    }

    # Always a list even for a mosaic
    first_acq = sorted(
        (d for d in series if int(d.AcquisitionNumber) == 1),
        key=lambda d: float(d.SliceLocation)
    )

    # NB: For now we reject scans that are not acquired in contiguous ascending order
    # as the 'SliceDelay' would need to be interpreted differently
    if MosaicRefAcqTimes in first_acq[0]:
        # Mosaic mode
        reftimes = [float(t) for t in first_acq[0][MosaicRefAcqTimes].value]
        if sorted(reftimes) != reftimes:
            raise ValueError('PCASL slices not acquired in ascending order')
        params['SliceDelay'] = float(np.mean(np.diff(reftimes)))  # millisecs
    else:
        # Single slice mode
        # All of the slices in just the first acquisition
        reftimes = [float(slc[TimeAfterStart].value) for slc in first_acq]
        if sorted(reftimes) != reftimes:
            raise ValueError('PCASL slices not acquired in ascending order')
        params['SliceDelay'] = float(1000 * np.mean(np.diff(reftimes)))  # millisecs
    return params


def basic_params(dobjs):
    '''A dictionary of useful basic acquisition parameters from a list of dicom objects.

    Parameters
    ----------
    dobjs :
        a list of dicom objects.
    '''

    # Sort by both acquisition number and slice position
    # (if these are mosaics then the slice positions will nominal and equal for all)
    series = sorted(dobjs, key=lambda d: (int(d.AcquisitionNumber), float(d.SliceLocation)))
    img = series[0] # still ok for mosaic
    params = {
        'RepetitionTime': float(img.RepetitionTime),  # millisecs
    }

    if 'InversionTime' in img:
        params['InversionTime'] = float(img.InversionTime)

    # Special handling for (multiple) echo times
    echo_times = sorted(set(float(img.EchoTime) for img in series))
    for i, echo_time in enumerate(echo_times):
        params['EchoTime%d' % (i+1)] = echo_time

    return params


def pcasl_params_from_dir(dname, glob_pattern='*'):
    '''Get a dictionary of useful pCASL acquisition parameters from dicom objects in a directory

    Parameters
    ----------
    dname :
        directory to find files.
    glob_pattern:
        glob pattern to match files
    '''

    fnames = glob(join(dname, glob_pattern))
    return pcasl_params([dcmread(f) for f in fnames])


def basic_params_from_dir(dname, glob_pattern='*'):
    '''Get a dictionary of basic acquisition parameters from dicom objects in a directory

    Parameters
    ----------
    dname :
        directory to find files.
    glob_pattern:
        glob pattern to match files
    '''

    fnames = glob(join(dname, glob_pattern))
    return basic_params([dcmread(f) for f in fnames])


def fetch_casl(server, patid, studyid, seriesnos, filenames, workingdir, isodate=None):
    '''Download a series of pCASL acquisitions from a dicom server.
       Converts series to nifti format using dcm2nii and extracts sequence
       parameters for use in analysis.

       Returns dictionary with details of scans.

    Parameters
    ----------
    server :
        DICOM server as specified in /etc/dcmnodes.cf
    patid:
        patient ID
    studyid:
        study ID - usually just '1'
    seriesnos:
       list of series numbers of casl acquistions at different PLDs
    filenames:
       list of filenames under which to save casl aquisitions
    workingdir:
       directory into which to download series
    isodate:
       optionally restrict to scans on a specific date
    '''

    # Get details of all the series for study
    qi = QueryInterface()
    if isodate is None:
        seriess = sorted([
            series for series in qi.combo_find(server, patid)
                if series.studyid == studyid
                ], key=attrgetter('seriesnumber')
        )
        if not seriess:
            raise ValueError('No series found for patient %s, study %s' % (patid, studyid))
    else:
        seriess = sorted([
            series for series in qi.combo_find(server, patid)
                if series.studyid == studyid and series.studydate == isodate
                ], key=attrgetter('seriesnumber')
        )
        if not seriess:
            raise ValueError('No series found for patient %s, study %s on %s' % (patid, studyid, isodate))

    params = []
    for seriesno, filename in zip(seriesnos, filenames):
        s_l = [ss for ss in seriess if ss.seriesnumber == seriesno]
        if s_l:
            s = s_l[0]
        else:
            raise ValueError('Series Number %d not found' % seriesno)

        tempdir = mkdtemp()
        list(qi.series_level_fetch(
                server,
                patid=s.patid, studyuid=s.studyuid, seriesuid=s.seriesuid,
                savedir=tempdir
            )
        )

        params.append(pcasl_params_from_dir(tempdir))
        example_dcm = glob(join(tempdir, '*'))[0]
        copyfile(example_dcm, join(workingdir, filename) + '.dcm')

        _convert_to_nifti(tempdir)
        _merge_to_timeseries(tempdir, workingdir, filename)

        rmtree(tempdir)

    return {
        'SeriesList': seriesnos, 'FileNames': filenames, 'ScanParameters': params
    }


def fetch_m0(server, patid, studyid, seriesno, filename, workingdir, isodate=None):
    '''Download pCASL M0 reference scan from a dicom server.
       Converts series to nifti format using dcm2nii and extracts sequence
       parameters for use in analysis.

       Returns dictionary with details of scan.

    Parameters
    ----------
    server :
        DICOM server as specified in /etc/dcmnodes.cf
    patid:
        patient ID
    studyid:
        study ID - usually just '1'
    seriesno:
       series numbers of casl M0 acquistion
    filename:
       filename under which to save M0 aquisition
    workingdir:
       directory into which to download series
    isodate:
       optionally restrict to scans on a specific date
    '''

    # Get details of all the series for study
    qi = QueryInterface()
    if isodate is None:
        seriess = sorted([
            series for series in qi.combo_find(server, patid)
                if series.studyid == studyid
                ], key=attrgetter('seriesnumber')
        )
    else:
        seriess = sorted([
            series for series in qi.combo_find(server, patid)
                if series.studyid == studyid and series.studydate == isodate
                ], key=attrgetter('seriesnumber')
        )

    s = [ss for ss in seriess if ss.seriesnumber == seriesno][0]
    tempdir = mkdtemp()
    list(qi.series_level_fetch(
            server,
            patid=s.patid, studyuid=s.studyuid, seriesuid=s.seriesuid,
            savedir=tempdir
        )
    )

    params = pcasl_params_from_dir(tempdir)
    example_dcm = glob(join(tempdir, '*'))[0]
    copyfile(example_dcm, join(workingdir, filename) + '.dcm')

    _convert_to_nifti(tempdir)
    _merge_to_timeseries(tempdir, workingdir, filename)

    rmtree(tempdir)

    return {
        'SeriesList': [seriesno], 'FileNames': [filename], 'ScanParameters': [params]
    }


fetch_m0norm = fetch_m0


def fetch_structural(server, patid, studyid, seriesno, filename, workingdir, isodate=None):
    '''Download structural reference scan from a dicom server.
       Converts series to nifti format using dcm2nii.

       Returns dictionary with details of scan.

    Parameters
    ----------
    server :
        DICOM server as specified in /etc/dcmnodes.cf
    patid:
        patient ID
    studyid:
        study ID - usually just '1'
    seriesno:
       series numbers of structural scan eg mprage
    filename:
       filename under which to save structural scan
    workingdir:
       directory into which to download series
    isodate:
       optionally restrict to scans on a specific date
    '''

    # Get details of all the series for study
    qi = QueryInterface()
    if isodate is None:
        seriess = sorted([
            series for series in qi.combo_find(server, patid)
                if series.studyid == studyid
                ], key=attrgetter('seriesnumber')
        )
    else:
        seriess = sorted([
            series for series in qi.combo_find(server, patid)
                if series.studyid == studyid and series.studydate == isodate
                ], key=attrgetter('seriesnumber')
        )

    s = [ss for ss in seriess if ss.seriesnumber == seriesno][0]
    tempdir = mkdtemp()
    list(qi.series_level_fetch(
            server,
            patid=s.patid, studyuid=s.studyuid, seriesuid=s.seriesuid,
            savedir=tempdir
        )
    )

    params = basic_params_from_dir(tempdir)
    example_dcm = glob(join(tempdir, '*'))[0]
    copyfile(example_dcm, join(workingdir, filename) + '.dcm')

    _convert_to_nifti(tempdir)

    move(glob(join(tempdir, '20*.nii.gz'))[0], join(workingdir, filename + '.nii.gz'))

    rmtree(tempdir)

    return {
        'SeriesList': [seriesno], 'FileNames': [filename], 'ScanParameters': [params]
    }


fetch_fm = fetch_structural
fetch_fmmag = fetch_structural


def show_mosaic(base_nifti, overlay_nifti=None, alpha=0.25, cmap='bone',
                overlay_cmap='coolwarm', title='Image Mosaic', colourbar=False, colourbar_label=None,
                vmin=0, vmax=None, stride=1, mask=False):
    '''Show two nifti image stacks as mosaics, the second overlaid on the first
       with the specified colourmap amd transparency. Optional colourbar for the base image.
       The images have to be the same shape.
       If second image is a mask then makes background transparent.

    Parameters
    ----------
    base_nifti: str or filelike
        filespec of underlying nifti image
    overlay_nifti: str or filelike
        filespec of (optional) overlay nifti image
    alpha: float
        transparency of overlay if full image rather than mask
    cmap: str or other rorm of cmap specification
       colour map for underlying image
    overlay_cmap: str
       colour map for overlay if full image rather than mask
    title: str
       title for plot
    colourbar: bool
        include colour bar in plot
    colourbar_label: str
        ilabel for units on colour bar
    vmin: float
        base of data range for underlying image (default 0)
    vmax: float
        top of data range for underlying image (default 95% of maximum)
    stride: int
        stride through stack - a stride of two means only display every other slice (default display every slice)
    mask: bool
        treat overlay as an opaque binary mask rather than a transparent overwash - (default False)
    '''

    # Base image stack
    base_image = nib.load(base_nifti)
    d_base = base_image.get_data().T
    nz, ny, nx = d_base.shape

    # Sparse display
    if stride > 1:
        d_base = d_base[::stride]

    # Image range
    vmin_base = vmin
    vmax_base = np.percentile(d_base, 95) if vmax is None else vmax

    # Construct single image mosaic/montage from slices
    nimages = len(d_base)
    ncols = int(round(np.sqrt(nimages)))
    nrows = int(np.ceil(nimages / ncols))
    if ncols == 0 or nrows == 0:
        nrows = ncols = 1

    montage = np.zeros((ny*nrows, nx*ncols))
    for i, image in enumerate(d_base):
        x, y = (i % ncols) * nx, (i // ncols) * ny
        montage[y:y+ny, x:x+nx] = np.flipud(image)

    # Construct montage for overlay if specified
    if overlay_nifti is not None:
        overlay_image = nib.load(overlay_nifti)
        d_overlay = overlay_image.get_data().T
        if stride > 1:
            d_overlay = d_overlay[::stride]
        if d_overlay.shape != d_base.shape:
            raise ValueError('Mismatch in shape between base image series and overlay')

        vmin_overlay, vmax_overlay = np.min(d_overlay), np.max(d_overlay)
        # construct overlay montage
        overlay_montage = np.zeros((ny*nrows, nx*ncols))
        for i, image in enumerate(d_overlay):
            x, y = (i % ncols) * nx, (i // ncols) * ny
            overlay_montage[y:y+ny, x:x+nx] = np.flipud(image)

    # Display images
    figsize = max(3*ncols, 6), max(3*nrows, 6)
    fig, ax = plt.subplots(1, 1, figsize=figsize)

    mplim = ax.imshow(montage, cmap=cmap, vmin=vmin_base, vmax=vmax_base)

    if overlay_nifti is not None:
        if mask:
            overlay_montage = np.ma.masked_where(overlay_montage <= 0.1, overlay_montage)
            ax.imshow(
                overlay_montage,
                cmap='coolwarm', vmin=0, vmax=1,
                interpolation='nearest'
            )
        else:
            ax.imshow(
                overlay_montage,
                cmap=overlay_cmap, vmin=vmin_overlay, vmax=vmax_overlay,
                alpha=alpha
            )

    ax.axis('off')

    #  Colour bar for image scale
    if colourbar:
        # Clip to height of image
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        fig.colorbar(mplim, cax=cax)
        cax.tick_params(labelsize=15)
        if colourbar_label:
            cax.set_ylabel(colourbar_label, size=20)

    # TODO: this is weird - seems to need double the font size we need in the notebook
    ax.set_title(title, fontsize=20)
    return


def slicer_png_report(ima, imb):
    '''Use fsl slicer to produce png image of datasets overlaid on each other
       to demonstrate registration as in the fsl web reports. Image is two rows.
       Each row has four representative slices along each axis. The images are a background
       greyscale image from one data set and a red edge image from the other. On the second
       row the two are reversed.

       Returns numpy array of png image data.

    Parameters
    ----------
    ima: str
        filename of first nifti data set
    imb: str
        filename of second nifti data set
    '''
    tmpdir = mkdtemp()

    dirns = ['x', 'y', 'z']
    posns = [0.35, 0.45, 0.55, 0.65]

    # a over b
    cmd = ['slicer', ima, imb, '-s', '2']
    for dirn in dirns:
        for posn in posns:
            cmd += ['-' + dirn, '%0.2f' % posn, join(tmpdir, 'sl%s%d.png' % (dirn, 100 * posn))]
    check_call(cmd)

    # assemble images into a composite horizontally with '+'
    cmd = ['pngappend']
    for dirn in dirns:
        for posn in posns:
            cmd += [join(tmpdir, 'sl%s%d.png' % (dirn, 100 * posn)), '+']
    cmd = cmd[:-1] + [join(tmpdir, 'row_one.png')]  # remove trailing '+'
    check_call(cmd)

    # b over a
    cmd = ['slicer', imb, ima, '-s', '2']
    for dirn in dirns:
        for posn in posns:
            cmd += ['-' + dirn, '%0.2f' % posn, join(tmpdir, 'sl%s%d.png' % (dirn, 100 * posn))]
    check_call(cmd)

    cmd = ['pngappend']
    for dirn in dirns:
        for posn in posns:
            cmd += [join(tmpdir, 'sl%s%d.png' % (dirn, 100 * posn)), '+']
    cmd = cmd[:-1] + [join(tmpdir, 'row_two.png')]
    check_call(cmd)

    # assemble images into a composite vertically with '-'
    cmd = [
        'pngappend',
        join(tmpdir, 'row_one.png'), '-', join(tmpdir, 'row_two.png'),
        join(tmpdir, 'both_rows.png'),
    ]
    check_call(cmd)

    with open(join(tmpdir, 'both_rows.png'), 'rb') as f:
        png_data = f.read()

    rmtree(tmpdir)

    return png_data
