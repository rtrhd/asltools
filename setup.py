try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Support for Arterial Spin Labelling Analysis at CRICBristol',
    'author': 'Ronald Hartley-Davies',
    'url': 'https://bitbucket.org/rtrhd/asltools',
    'download_url': 'https://bitbucket.org/rtrhd/asltools/downloads',
    'author_email': 'R.Hartley-Davies@bristol.ac.uk',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['asltools'],
    'scripts': [],
    'name': 'asltools'
}

setup(**config)

